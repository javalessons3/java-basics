// OPERATORY PORÓWNANIA
/*
==   równy
!=   różny
>=   większy lub równy
<=   mniejszy lub równy
>    większy
<    mniejszy
 */

 public class OperatoryPorownania {
    public static void main(String[] args) {

        int numberA = 10;
        int numberB = 3;

        boolean result = numberA > numberB;
        System.out.println(result);           // true

        result = numberA == numberB;
        System.out.println(result);           // false

        result = numberA != numberB;
        System.out.println(result);           // true

        result = numberA >= numberB;
        System.out.println(result);           // true

        result = numberA <= numberB;
        System.out.println(result);           // false

        result = numberA < numberB;
        System.out.println(result);           // false

        result = numberA > numberB;
        System.out.println(result);           // true

    // Zapis z mniejszą ilością kodu

        int numberC = 10;
        int numberD = 3;

        boolean result2 = numberC > numberD;   // true

        System.out.println(result2);
        System.out.println("C = D :" + (numberC == numberD));      // false
        System.out.println("C ! D :" + (numberC != numberD));      // true
        System.out.println("C >= D :" + (numberC >= numberD));     // true
        System.out.println("C <+ D :" + (numberC <= numberD));     // false
        System.out.println("C > D :" + (numberC > numberD));       // true
        System.out.println("C < D :" + (numberC < numberD));       // false
    }
}
