// ZMIENNA: 
// Posiada typ (rodzaj informacji, jaką przechowuje), wartość (informację jaką przechowuje) oraz nazwę. 
// Zmienną można również rozumieć jako pudełko w którym trzymamy wartości.

public class Zmienne {
    public static void main(String[] args) {

        String name = "Adam";
//      name = "Sylwia";     // - można nadpisać i wyświetli się Sylwia

        System.out.println(name);


        int firstNumber = 3;
        int secondNumber = 6;
        int result = firstNumber + secondNumber;

        System.out.println(result);
    }

}

/*
Możemy wywołać też w ten sposób:

        int firstNumber, secondNumber, result;

        firstNumber = 3;
        secondNumber = 6;
        result = firstNumber + secondNumber;

LUB

        int firstNumber;
        int secondNumber;
        int result;

        firstNumber = 3;
        secondNumber = 6;
        result = firstNumber + secondNumber;

 */
